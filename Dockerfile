FROM python:3.9.6 as build
ENV VAR=80
WORKDIR /multi-stage
RUN go build ./multi-stage/hw9.py -o out

FROM nginx:1.21.1
COPY --from=build /out .
CMD ["./out"]