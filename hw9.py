# encoding: utf-8
import datetime


def print_hi():
    days_file = open('hw9.txt', 'w')
    days_file.write(f"Время генерации текстового файла: {':'.join(str(datetime.datetime.now()).split(':')[:-1])}")
    days_file.close()


if __name__ == '__main__':
    print_hi()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
