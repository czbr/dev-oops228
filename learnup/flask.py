import psycopg2
from flask import Flask, render_template
from contextlib import closing
import json


app = Flask(__name__)


@app.route("/")
def index():
    return "Hello World!"


@app.route("/db")
def db1():
    dict_sample = {}
    with closing(psycopg2.connect(dbname='Claim', user='postgres',
                                  password='2281488', host='localhost')) as conn:
        with conn.cursor() as cursor:
            cursor.execute('SELECT * FROM type_humans')
            with open("data.json", "w", encoding="utf-8") as file:
                for row in cursor:
                    dict_sample[str(row[0])] = str(row[1])
                json.dump(dict_sample, file)
    return dict_sample


if __name__ == "__main__":
    app.run(debug=True)
